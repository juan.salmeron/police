import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import sys
import tensorflow as tf
import cv2
import time
from PIL import Image
from object_detection.utils import ops as utils_ops
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

sys.path.append('tracker/')
import PTracker
from PTracker import PersonTracker
from PTracker import PersonDetector

tf.gfile = tf.io.gfile
#Show Images
Show_cam=True
#Edit if you want to add traking and marking to frames
WRITE_BOXES=True
#If the previous is False it doesn't matter
WRITE_TRACKERS=False
#In what certainty to invoke the function
cop_precentage_invoke=0.8#every cop over 80% will invoke COPS_detected()
def COPS_detected():
  print("Do something like call me or activate pango!")
  #then wait like 15 minutes
  time.sleep(15*60)
  print("Do somthing else like deactivate pango?")

def load_model(model_name):
  model_dir = 'models/persons/' + model_name + "/saved_model"
  print(str(model_dir))
  model = tf.saved_model.load(str(model_dir))
  model = model.signatures['serving_default']
  return model

PATH_TO_LABELS = 'models/persons/mscoco_label_map.pbtxt'
category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

model_name = 'ssd_mobilenet_v2_coco_2018_03_29'
print("Loading persons model...")
detection_model = load_model(model_name)
print("Model loaded!")
filepathes=[]
sizes=[]
filepathes+=['models/cop_class/0.92770_L_0.35915_opt_RMSprop_loss_b_crossentropy_lr_0.0005_baches_20_shape_[165, 90]_loss.hdf5']
sizes+=[(90,165)]
####################
#Tracker
Trackers = []

def run_inference_for_single_image(model, image,imgname):
  input_tensor = tf.convert_to_tensor(image)
  input_tensor = input_tensor[tf.newaxis,...]
  try:
    output_dict = model(input_tensor)
  except:
    print("{} faild!".format(str(image)))
    return None

  num_detections = int(output_dict.pop('num_detections'))
  output_dict = {key:value[0, :num_detections].numpy() 
                 for key,value in output_dict.items()}
  output_dict['num_detections'] = num_detections
  output_dict['detection_classes'] = output_dict['detection_classes'].astype(np.int64)
  
  # Handle models with masks:
  if 'detection_masks' in output_dict:
    # Reframe the the bbox mask to the image size.
    detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
              output_dict['detection_masks'], output_dict['detection_boxes'],
               image.shape[0], image.shape[1])      
    detection_masks_reframed = tf.cast(detection_masks_reframed > 0.5,
                                       tf.uint8)
    output_dict['detection_masks_reframed'] = detection_masks_reframed.numpy()
  return output_dict
def show_inference(model, image_frame,frm_num):
  global Trackers
  image_np=np.array(image_frame)
  # Actual detection.
  output_dict = run_inference_for_single_image(model, image_np,image_frame)
  if not output_dict:
    #update trakers
    for trk in Trackers:
      trk.update(image_frame)
      #since if update successful fails=fails-0.1 
      trk.fails+=1.1
    Trackers = [tr for tr in Trackers if not tr.destroy()]
    return
  
  y,x,z = image_np.shape
  extract_persons(image_np,output_dict,image_frame)
  Detections=[]
  for ind,i in enumerate(output_dict['persons']):
    if output_dict['detection_scores'][i]>0.5:
      Detections+=[PersonDetector(output_dict['detection_scores'][i],output_dict['persons_class_score'][ind],output_dict['cv_boxes'][ind],output_dict['detection_boxes'][i])]
  Detections,Trackers =PTracker.assign_detections_to_trackers(Trackers, Detections,image_frame)
  for det in Detections:
    if det.clas==91 and det.tot_scor>=cop_precentage_invoke:
      COPS_detected()
      break
  #write data on the frame
  if WRITE_BOXES:
    after_trck_dict={'detection_id':[],'detection_boxes':[],'detection_classes':[],'detection_scores':[]}
    if WRITE_TRACKERS:
      for det in Trackers:
        if det.ok:
          after_trck_dict['detection_id']+=[-2]
          after_trck_dict['detection_boxes']+=[PTracker.cv2_bbox_to_tf(det.bbox,x,y)]
          after_trck_dict['detection_classes']+=[2]
          after_trck_dict['detection_scores']+=[1]
          
    for det in Detections:
      after_trck_dict['detection_id']+=[det.id]
      after_trck_dict['detection_boxes']+=[det.tf_bbox]
      after_trck_dict['detection_classes']+=[det.clas]
      after_trck_dict['detection_scores']+=[det.tot_scor]
    
    after_trck_dict['detection_boxes']=np.array(after_trck_dict['detection_boxes'])
    
    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        after_trck_dict['detection_boxes'],
        after_trck_dict['detection_classes'],
        after_trck_dict['detection_scores'],
        category_index,
        track_ids=after_trck_dict['detection_id'],
        instance_masks=output_dict.get('detection_masks_reframed', None),
        use_normalized_coordinates=True,
        line_thickness=1,
        min_score_thresh=0.01)
  return image_np
  
def extract_persons(image_np,output_dict,image_frame):
  output_dict['persons']=[]
  output_dict['persons_class_score']=[]
  output_dict['cv_boxes']=[]
  y,x,z = image_np.shape
  for i in range(len(output_dict['detection_classes'])):
    if output_dict['detection_classes'][i]==1 and output_dict['detection_scores'][i]>0.5:
      
      bx=output_dict['detection_boxes'][i]
      box=[int(bx[0]*y),int(bx[2]*y),int(bx[1]*x),int(bx[3]*x)]
      box[0]=0 if box[0]<0 else box[0]
      box[1]=y-1 if box[1]>y-1 else box[1]
      box[2]=0 if box[2]<0 else box[2]
      box[3]=x-1 if box[3]>x-1 else box[3]
      wd=box[3]-box[2]
      yd=box[1]-box[0]
      crop=image_np[box[0]:box[1],box[2]:box[3]]
      img=Image.fromarray(crop).resize(IMAGE_SHAPE)
      img=np.array(img)/255.0
      res=model_cp.predict(img[np.newaxis, ...])
      res=res[0][0]
      output_dict['persons_class_score']+=[res]
      output_dict['persons']+=[i]
      output_dict['cv_boxes']+=[(box[2],box[0],wd,yd)]
    else:
      output_dict['detection_scores'][i]=0

      

def run_vid(trim):
  cap = cv2.VideoCapture(0)
  PTracker.trk_id=0
  frame_number = -1
  fps=(cap.get(cv2.CAP_PROP_FPS))
  h=int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
  w=int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
  print(f"h:{h} w:{w} fps:{fps}")
  
  if(w>480):
    t=480/w
  else:
    t=1
  dst_size=(int(t*w),int(t*h))
  while(cap.isOpened()):
    frame_number += 1
    ret, frame = cap.read()
    if not ret:
      break
    new_frm=frame
    if(trim):
      new_frm=cv2.resize(new_frm,dst_size,fx=0,fy=0, interpolation = cv2.INTER_LINEAR)
    new_frm=cv2.cvtColor(new_frm, cv2.COLOR_BGR2RGB)
    new_frm=show_inference(detection_model,new_frm,frame_number)
    if Show_cam:
      if not trim:
        new_frm=cv2.resize(new_frm,dst_size,fx=0,fy=0, interpolation = cv2.INTER_LINEAR)
      new_frm=cv2.cvtColor(new_frm, cv2.COLOR_RGB2BGR)
      
      cv2.imshow('frame',new_frm)
      
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    
  cap.release()
  cv2.destroyAllWindows()
  print("Done!")



print("Loading cops model...")
model_cp=tf.keras.models.load_model(filepathes[0], custom_objects=None, compile=True)
IMAGE_SHAPE=sizes[0]
print("Model loaded!")
run_vid(1)
